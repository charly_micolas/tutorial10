package com.example;
/*
 * Charly Micolas Butarbutar
 * 1606954773
 * */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdbTutorial04Application
{

    public static void main (String[] args)
    {
        SpringApplication.run (PdbTutorial04Application.class, args);
    }
}
