package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTest {
	@Autowired
	StudentService service;
	
	@Test
	public void testSelectAllStudents()
	{
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 3, students.size());
	}
	
	@Test
	public void testSelectStudent()
	{
		StudentModel student = service.selectStudent("127");
		
		// Mahasiswa yang dikembalikan tidak null
		Assert.assertNotNull("Gagal = Student null", student);
		
		// Nama mahasiswa sesuai dengan nama yang ada di database
		Assert.assertEquals("Gagal - Nama tidak sesuai", "Budis", student.getName());
		
		// GPA mahasiswa sesuai dengan GPA yang ada di database
		Assert.assertEquals("Gagal - GPA tidak sesuai", 3.5, student.getGpa(),0.0);
		
	}
	
	
	@Test
	public void testCreateStudent()
	{
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		
		// Masukkan ke service
		service.addStudent(student);
		
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testupdateStudent()
	{
		StudentModel student = service.selectStudent("127");
		student.setGpa(3.8);
		service.updateStudent(student);
		student = service.selectStudent("127");
		
		// Cek apakah attribut berubah
		Assert.assertEquals("GPA tidak berubah", 3.8, student.getGpa(), 0.0);
		
	}
	
	
	@Test
	public void testDeleteStudent()
	{
		StudentModel student = service.selectStudent("127");
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		service.deleteStudent(student.getNpm());
		student = service.selectStudent("127");
		Assert.assertNull("Gagal = student tidak null", student);

	}
	
}
